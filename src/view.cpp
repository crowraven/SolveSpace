//-----------------------------------------------------------------------------
// The View menu, stuff to snap to certain special vews of the model, and to
// display our current view of the model to the user.
//
// Copyright 2008-2013 Jonathan Westhues.
//-----------------------------------------------------------------------------
#include "solvespace.h"

void TextWindow::ShowEditView(void) {
    Printf(true, "%Ft三维显示参数%E");

    Printf(true,  "%Bd %Ft总比例尺%E");
    Printf(false, "%Ba   %# px/%s %Fl%Ll%f[修改]%E",
        SS.GW.scale * SS.MmPerUnit(),
        SS.UnitName(),
        &ScreenChangeViewScale);
    Printf(false, "");

    Printf(false, "%Bd %Ft原点 (映射到屏幕中心)%E");
    Printf(false, "%Ba   (%s, %s, %s) %Fl%Ll%f[修改]%E",
        SS.MmToString(-SS.GW.offset.x).c_str(),
        SS.MmToString(-SS.GW.offset.y).c_str(),
        SS.MmToString(-SS.GW.offset.z).c_str(),
        &ScreenChangeViewOrigin);
    Printf(false, "");

    Vector n = (SS.GW.projRight).Cross(SS.GW.projUp);
    Printf(false, "%Bd %Ft投影到屏幕%E");
    Printf(false, "%Ba   %Ft右%E (%3, %3, %3) %Fl%Ll%f[修改]%E",
        CO(SS.GW.projRight),
        &ScreenChangeViewProjection);
    Printf(false, "%Bd   %Ft上%E (%3, %3, %3)", CO(SS.GW.projUp));
    Printf(false, "%Ba   %Ft外%E (%3, %3, %3)", CO(n));
    Printf(false, "");

    Printf(false, "透视图可以在配置中更改.");
}

void TextWindow::ScreenChangeViewScale(int link, uint32_t v) {
    SS.TW.edit.meaning = EDIT_VIEW_SCALE;
    SS.TW.ShowEditControl(3, ssprintf("%.3f", SS.GW.scale * SS.MmPerUnit()));
}

void TextWindow::ScreenChangeViewOrigin(int link, uint32_t v) {
    std::string edit_value =
        ssprintf("%s, %s, %s",
            SS.MmToString(-SS.GW.offset.x).c_str(),
            SS.MmToString(-SS.GW.offset.y).c_str(),
            SS.MmToString(-SS.GW.offset.z).c_str());

    SS.TW.edit.meaning = EDIT_VIEW_ORIGIN;
    SS.TW.ShowEditControl(4, edit_value);
}

void TextWindow::ScreenChangeViewProjection(int link, uint32_t v) {
    std::string edit_value =
        ssprintf("%.3f, %.3f, %.3f", CO(SS.GW.projRight));
    SS.TW.edit.meaning = EDIT_VIEW_PROJ_RIGHT;
    SS.TW.ShowEditControl(8, edit_value);
}

bool TextWindow::EditControlDoneForView(const char *s) {
    switch(edit.meaning) {
        case EDIT_VIEW_SCALE: {
            Expr *e = Expr::From(s, true);
            if(e) {
                double v =  e->Eval() / SS.MmPerUnit();
                if(v > LENGTH_EPS) {
                    SS.GW.scale = v;
                } else {
                    Error("比例不能为零或负数。");
                }
            }
            break;
        }

        case EDIT_VIEW_ORIGIN: {
            Vector pt;
            if(sscanf(s, "%lf, %lf, %lf", &pt.x, &pt.y, &pt.z) == 3) {
                pt = pt.ScaledBy(SS.MmPerUnit());
                SS.GW.offset = pt.ScaledBy(-1);
            } else {
                Error("格式不正确：指定 x, y, z");
            }
            break;
        }

        case EDIT_VIEW_PROJ_RIGHT:
        case EDIT_VIEW_PROJ_UP: {
            Vector pt;
            if(sscanf(s, "%lf, %lf, %lf", &pt.x, &pt.y, &pt.z) != 3) {
                Error("格式不正确：指定 x, y, z");
                break;
            }
            if(edit.meaning == EDIT_VIEW_PROJ_RIGHT) {
                SS.GW.projRight = pt;
                SS.GW.NormalizeProjectionVectors();
                edit.meaning = EDIT_VIEW_PROJ_UP;
                HideEditControl();
                ShowEditControl(10, ssprintf("%.3f, %.3f, %.3f", CO(SS.GW.projUp)),
                                editControl.halfRow + 2);
                edit.showAgain = true;
            } else {
                SS.GW.projUp = pt;
                SS.GW.NormalizeProjectionVectors();
            }
            break;
        }

        default:
            return false;
    }
    return true;
}

